const discord = require('discord.js')
require('dotenv').config()

const client = new discord.Client()

client.on('ready', () => {
    client.user.setPresence({ activity: { name: 'with you | fragmented.group' }, status: 'dnd'})
})

client.login(process.env.TOKEN)
